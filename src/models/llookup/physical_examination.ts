import { Knex } from 'knex';
export class PhysicalExaminationModel {

  info(db: Knex) {
    let sql = db('l_physical_examination')
    return sql.where('is_active',true).orderBy('name')
  }

  infoTotal(db: Knex) {
    let sql = db('l_physical_examination')
    return sql.where('is_active',true).count({ total: '*' });
  }
  
  infoByID(db: Knex,id:any) {
    let sql = db('l_physical_examination')
    return sql.where('is_active',true).andWhere('id',id)
    .orderBy('name')
  } 

}