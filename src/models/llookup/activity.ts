import { Knex } from 'knex';
export class ActivityModel {
  /**
   * บันทึกข้อมูล Admit
   * @param db 
   * @param data 
   * @returns 
   */
  info(db: Knex) {
    let sql = db('l_activity')
    return sql.where('is_active',true).orderBy('name')
  }

  infoTotal(db: Knex) {
    let sql = db('l_activity')
    return sql.where('is_active',true).count({ total: '*' });
  }
  
  infoByID(db: Knex,id:any) {
    let sql = db('l_activity')
    return sql.where('is_active',true)
    .andWhere('id',id)
    .orderBy('name')
  } 

}