import { Knex } from 'knex';
export class CouncilModel {
  /**
   * บันทึกข้อมูล Admit
   * @param db 
   * @param data 
   * @returns 
   */
  info(db: Knex) {
    let sql = db('l_council')
    return sql.where('is_active',true).orderBy('code')
  }

  infoTotal(db: Knex) {
    let sql = db('l_council')
    return sql.where('is_active',true).count({ total: '*' });
  }
  
  infoByID(db: Knex,id:any) {
    let sql = db('l_council')
    return sql.where('is_active',true)
    .andWhere('id',id)
    .orderBy('code')
  } 

}