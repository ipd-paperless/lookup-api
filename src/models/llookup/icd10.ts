import { Knex } from 'knex';
export class Icd10Model {

  info(db: Knex) {
    let sql = db('l_icd10')
    return sql.where('is_active',true).orderBy('code')
  }

  infoTotal(db: Knex) {
    let sql = db('l_icd10')
    return sql.where('is_active',true).count({ total: '*' });
  }
  
  infoByID(db: Knex,id:any) {
    let sql = db('l_icd10')
    return sql.where('is_active',true).andWhere('id',id)
    .orderBy('code')
  } 

}