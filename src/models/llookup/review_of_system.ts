import { Knex } from 'knex';
export class ReviewOfSystemModel {

  info(db: Knex) {
    let sql = db('l_review_of_system')
    return sql.where('is_active',true).orderBy('name')
  }

  infoTotal(db: Knex) {
    let sql = db('l_review_of_system')
    return sql.where('is_active',true).count({ total: '*' });
  }
  
  infoByID(db: Knex,id:any) {
    let sql = db('l_review_of_system')
    return sql.where('is_active',true).andWhere('id',id)
    .orderBy('name')
  } 

}