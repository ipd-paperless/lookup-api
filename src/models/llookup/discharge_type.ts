import { Knex } from 'knex';
export class DischargeTypeModel {
/**
 * 
 * @param db 
 * @returns 
 */
  info(db: Knex) {
    let sql = db('l_discharge_type')
    return sql.where('is_active',true).orderBy('code')
  }

  infoTotal(db: Knex) {
    let sql = db('l_discharge_type')
    return sql.where('is_active',true).count({ total: '*' });
  }
  
  infoByID(db: Knex,id:any) {
    let sql = db('l_discharge_type')
    return sql.where('is_active',true).andWhere('id',id)
    .orderBy('code')
  } 

}