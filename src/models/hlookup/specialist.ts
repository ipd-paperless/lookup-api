import { UUID } from 'crypto';
import { Knex } from 'knex';
export class SpecialistModel {
  /**
   * บันทึกข้อมูล Admit
   * @param db 
   * @param data 
   * @returns 
   */
  
  list(db: Knex) {
    let sql = db('h_specialist')
    return sql.orderBy('name')
  }  
  
  info(db: Knex) {
    let sql = db('h_specialist')
    return sql.where('is_active',true)
    .orderBy('name')
  }

  infoTotal(db: Knex) {
    let sql = db('h_specialist')
    return sql.where('is_active',true)
    .count({ total: '*' });
  }

  infoByID(db: Knex,id:UUID) {
    let sql = db('h_specialist')
    return sql.where('id',id).orderBy('name')
  }

  save(db: Knex,data:object){
    let sql = db('h_specialist')
    return sql.insert(data);
  }

  update(db: Knex,data:object,id:UUID){
    let sql = db('h_specialist')
    return sql.update(data).where('id',id);
  }

  delete(db: Knex,id:UUID){
    let sql = db('h_specialist')
    return sql.delete().where('id',id);
  }  
}