import { UUID } from 'crypto';
import { Knex } from 'knex';
export class DoctorModel {
  /**
   * บันทึกข้อมูล Admit
   * @param db 
   * @param data 
   * @returns 
   */
  
  info(db: Knex) {
    let sql = db('user')
    // return sql.where('is_active',true).orderBy('fname')
    return sql.select('profile.*','user.roles','user.last_login_at','provider.name as provider_name')
            .innerJoin('profile','profile.user_id','user.id')
            .leftJoin('provider','provider.id','profile.provider_id')
            .where('user.is_active',true)
            .andWhere('provider.council_code','01')
  }

  infoTotal(db: Knex) {
    let sql = db('user')
    return sql.innerJoin('profile','profile.user_id','user.id')
            .leftJoin('provider','provider.id','profile.provider_id')
            .where('user.is_active',true)
            .andWhere('provider.council_code','01').count({ total: '*' });
  }
  

  infoByID(db: Knex,id:UUID) {
    let sql = db('user')
    // return sql.where('is_active',true).orderBy('fname')
    return sql.select('profile.*','user.roles','user.last_login_at','provider.name as provider_name')
            .innerJoin('profile','profile.user_id','user.id')
            .leftJoin('provider','provider.id','profile.provider_id')
            .where('user.is_active',true)
            .andWhere('provider.council_code','01')
            .andWhere('profile.id',id)
  }
}