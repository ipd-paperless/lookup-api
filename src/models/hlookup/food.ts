import { UUID } from 'crypto';
import { Knex } from 'knex';
export class FoodModel {
/**
 * 
 * @param db 
 * @returns 
 */

  list(db: Knex) {
    let sql = db('h_food')
    return sql.orderBy('code')
  }

  info(db: Knex) {
    let sql = db('h_food')
    return sql.where('is_active',true).orderBy('code')
  }

  infoTotal(db: Knex) {
    let sql = db('h_food')
    return sql.where('is_active',true).count({ total: '*' });
  }
  
  infoByID(db: Knex,id:any) {
    let sql = db('h_food')
    return sql.where('is_active',true).andWhere('id',id).orderBy('name')
  } 

  save(db: Knex,data:object){
    let sql = db('h_food')
    return sql.insert(data);
  }

  update(db: Knex,data:object,id:UUID){
    let sql = db('h_food')
    return sql.update(data).where('id',id);
  }

  delete(db: Knex,id:UUID){
    let sql = db('h_food')
    return sql.delete().where('id',id);
  }  
}