import { UUID } from 'crypto';
import { Knex } from 'knex';
export class WardModel {
  /**
   * บันทึกข้อมูล Admit
   * @param db 
   * @param data 
   * @returns 
   */
  
  list(db: Knex) {
    let sql = db('h_ward')
    return sql.orderBy('name')
  }

  info(db: Knex) {
    let sql = db('h_ward')
    return sql.where('is_active',true).orderBy('name')
  }

  infoTotal(db: Knex) {
    let sql = db('h_ward')
    return sql.where('is_active',true).count({ total: '*' });
  }


  infoByID(db: Knex,id:UUID) {
    let sql = db('h_ward')
    return sql.where('is_active',true).where('id',id).orderBy('name')
  }

  infoByDepartmentId(db: Knex,department_id:UUID) {
    let sql = db('h_ward')
    return sql.where('is_active',true).where('department_id',department_id).orderBy('name')
  }

  save(db: Knex,data:object){
    let sql = db('h_ward')
    return sql.insert(data);
  }

  update(db: Knex,data:object,id:UUID){
    let sql = db('h_ward')
    return sql.update(data).where('id',id);
  }

  delete(db: Knex,id:UUID){
    let sql = db('h_ward')
    return sql.delete().where('id',id);
  }  
}