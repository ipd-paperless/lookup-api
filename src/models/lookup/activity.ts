import { UUID } from 'crypto';
import { Knex } from 'knex';
export class ActivityModel {

  list(db: Knex) {
    let sql = db('activity')
    return sql.orderBy('description')
  }

  info(db: Knex) {
    let sql = db('activity')
    return sql.where('is_active',true).orderBy('description')
  }

  infoTotal(db: Knex) {
    let sql = db('activity')
    return sql.where('is_active',true).count({ total: '*' });
  }

  infoByID(db: Knex,id:any) {
    let sql = db('activity')
    return sql.where('is_active',true).andWhere('id',id).orderBy('description')
  } 

  save(db: Knex,data:object){
    let sql = db('activity')
    return sql.insert(data);
  }

  update(db: Knex,data:object,id:UUID){
    let sql = db('activity')
    return sql.update(data).where('id',id);
  }

  delete(db: Knex,id:UUID){
    let sql = db('activity')
    return sql.delete().where('id',id);
  }
}