import { UUID } from 'crypto';
import { Knex } from 'knex';
export class StandingProgressNoteModel {
  /**
   * บันทึกข้อมูล Admit
   * @param db 
   * @param data 
   * @returns 
   */
  list(db: Knex) {
    let sql = db('standing_progress_note')
    return sql.orderBy('group_disease_id','department_id')
  }  
  
  info(db: Knex) {
    let sql = db('standing_progress_note')
    return sql.where('is_active',true).orderBy('group_disease_id','department_id')
  }

  infoGroupDiseaseID(db: Knex,group_disease_id:UUID) {
    let sql = db('standing_progress_note')
    return sql.where('is_active',true)
    .andWhere('group_disease_id',group_disease_id)
    .orderBy('group_disease_id','department_id')
  }

  infoTotal(db: Knex) {
    let sql = db('standing_progress_note')
    return sql.where('is_active',true).count({ total: '*' });
  }

  infoByID(db: Knex,id:UUID) {
    let sql = db('standing_progress_note')
    return sql.where('is_active',true).where('id',id).orderBy('group_disease_id','department_id')
  }

  save(db: Knex,data:object){
    let sql = db('standing_progress_note')
    return sql.insert(data);
  }

  update(db: Knex,data:object,id:UUID){
    let sql = db('standing_progress_note')
    return sql.update(data).where('id',id);
  }

  delete(db: Knex,id:UUID){
    let sql = db('standing_progress_note')
    return sql.delete().where('id',id);
  }  
}