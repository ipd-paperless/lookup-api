import { UUID } from 'crypto';
import { Knex } from 'knex';
export class UserModel {

  list(db: Knex) {
    let sql = db('user')
    return sql.orderBy('username')
  }

  info(db: Knex) {
    let sql = db('user')
    return sql.where('is_active',true).orderBy('username')
  }

  infoTotal(db: Knex) {
    let sql = db('user')
    return sql.where('is_active',true).count({ total: '*' });
  }

  infoByID(db: Knex,id:any) {
    let sql = db('user')
    return sql.where('is_active',true).andWhere('id',id).orderBy('username')
  } 

  infoByIDProfile(db: Knex,id:any) {
    let sql = db('profile')
    return sql.where('user_id',id)
  } 

  save(db: Knex,data:object){
    let sql = db('user')
    return sql.insert(data);
  }

  update(db: Knex,data:object,id:UUID){
    let sql = db('user')
    return sql.update(data).where('id',id);
  }

  delete(db: Knex,id:UUID){
    let sql = db('user')
    return sql.delete().where('id',id);
  }
}