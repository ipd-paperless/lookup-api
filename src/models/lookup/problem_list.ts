import { UUID } from 'crypto';
import { Knex } from 'knex';
export class ProblemListModel {
  /**
   * บันทึกข้อมูล Admit
   * @param db 
   * @param data 
   * @returns 
   */
  list(db: Knex) {
    let sql = db('problem_list')
    return sql.orderBy('description')
  }  
  
  info(db: Knex) {
    let sql = db('problem_list')
    return sql.where('is_active',true).orderBy('description')
  }

  infoTotal(db: Knex) {
    let sql = db('problem_list')
    return sql.where('is_active',true).count({ total: '*' });
  }

  infoByID(db: Knex,id:UUID) {
    let sql = db('problem_list')
    return sql.where('is_active',true).where('id',id).orderBy('description')
  }

  save(db: Knex,data:object){
    let sql = db('problem_list')
    return sql.insert(data);
  }

  update(db: Knex,data:object,id:UUID){
    let sql = db('problem_list')
    return sql.update(data).where('id',id);
  }

  delete(db: Knex,id:UUID){
    let sql = db('problem_list')
    return sql.delete().where('id',id);
  }  
}