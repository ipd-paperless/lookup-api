import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';

// schema
import createSchema from '../../schema/admit/create';
import { MedicineUsageModel } from '../../models/hlookup/medicine_usage';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const importService: any = new MedicineUsageModel()

  //Info
  fastify.get('/', {
    preHandler: [
      fastify.guard.role('admin','doctor','nurse'),
      // fastify.guard.scope('order.create','admit.read')
    ],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const query: any = request.query
      const search: any = query.search || null
      const _limit: number = query.limit || 50
      const _offset: number = query.offset || 0

      const data: any = await importService.list(db, search, _limit, _offset);
      const rsTotal: any = await importService.listTotal(db, search);

      return reply.status(StatusCodes.OK).send({ 
        ok: true, 
        data, 
        total: Number(rsTotal[0].total) 
      });
    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })
      //Save
      fastify.post('/', {
        preHandler: [
          fastify.guard.role('admin','doctor','nurse'),
          // fastify.guard.scope('order.create','admit.read')
        ],
      }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req:any = request;
        const datas:object = req.body;
        try {
          const data:any = await importService.save(db,datas);
          return reply.status(StatusCodes.OK).send({ ok: true ,data});
        } catch (error: any) {
          request.log.info(error.message);
          return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
        }
      })  
    
      //Update
      fastify.put('/:id', {
        preHandler: [
          fastify.guard.role('admin','doctor','nurse'),
          // fastify.guard.scope('order.create','admit.read')
        ],
      }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req:any = request;
        const datas:object = req.body;
        const id:any = req.params.id;
    
        try {
          const data:any = await importService.update(db,datas,id);
          return reply.status(StatusCodes.OK).send({ ok: true ,data});
        } catch (error: any) {
          request.log.info(error.message);
          return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
        }
      })  
    
      //Delete
      fastify.delete('/:id', {
        preHandler: [
          fastify.guard.role('admin','doctor','nurse'),
          // fastify.guard.scope('order.create','admit.read')
        ],
      }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req:any = request;
        const id:any = req.params.id;
    
        try {
          const data:any = await importService.delete(db,id);
          return reply.status(StatusCodes.OK).send({ ok: true ,data});
        } catch (error: any) {
          request.log.info(error.message);
          return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
        }
      }) 
    
      done();
    }