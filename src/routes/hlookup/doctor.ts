import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';
import { DoctorModel } from '../../models/hlookup/doctor';

// schema
import createSchema from '../../schema/admit/create';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const importService = new DoctorModel();

  //Info
  fastify.get('/', {
    preHandler: [
      fastify.guard.role('admin','doctor','nurse'),
      // fastify.guard.scope(['order.create','admit.read'])
    ],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {

    
      const data:any = await importService.info(db);
      const rsTotal: any = await importService.infoTotal(db);

      return reply.status(StatusCodes.OK).send({ ok: true ,data,total: Number(rsTotal[0].total)});

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })

  fastify.get('/infoByID/:id', {
    preHandler: [
      fastify.guard.role('admin','doctor','nurse'),
      // fastify.guard.scope(['order.create','admit.read'])
    ],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const req:any =  request;
      const id:any = await req.params.id;
    
      const data:any = await importService.infoByID(db,id);

      return reply.status(StatusCodes.OK).send({ ok: true ,data});

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })

  done();

} 
