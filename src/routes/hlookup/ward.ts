import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';
import { WardModel } from '../../models/hlookup/ward';

// schema
import createSchema from '../../schema/admit/create';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const importService = new WardModel();

  //List
  fastify.get('/list', {
    preHandler: [
      fastify.guard.role('admin','doctor','nurse'),
      // fastify.guard.scope('order.create','admit.read')
    ],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const data:any = await importService.list(db);
      return reply.status(StatusCodes.OK).send({ ok: true ,data});
    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })  

  //Info
  fastify.get('/', {
    preHandler: [
      fastify.guard.role('admin','doctor','nurse'),
      // fastify.guard.scope('order.create','admit.read')
    ],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const data:any = await importService.info(db);
      const rsTotal: any = await importService.infoTotal(db);
      return reply.status(StatusCodes.OK).send({ ok: true ,data,total: Number(rsTotal[0].total)});
    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })

  //Info By Id
  fastify.get('/:ward_id', {
    preHandler: [
      fastify.guard.role('admin','doctor','nurse'),
      // fastify.guard.scope('order.create','admit.read')
    ],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    const id:any = req.params.ward_id;
    try {
      const data:any = await importService.infoByID(db,id);
      return reply.status(StatusCodes.OK).send({ ok: true ,data});
    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })

  //List By DepartmentID
  fastify.get('/:department_id/department', {
    preHandler: [
      fastify.guard.role('admin','doctor','nurse'),
      // fastify.guard.scope('order.create','admit.read')
    ],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    const id:any = req.params.department_id;
    try {
      const data:any = await importService.infoByDepartmentId(db,id);

      return reply.status(StatusCodes.OK).send({ ok: true ,data});

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })

  //Save
  fastify.post('/', {
    preHandler: [
      fastify.guard.role('admin','doctor','nurse'),
      // fastify.guard.scope('order.create','admit.read')
    ],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    const datas:object = req.body;
    try {
      const data:any = await importService.save(db,datas);
      return reply.status(StatusCodes.OK).send({ ok: true ,data});
    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })  

  //Update
  fastify.put('/:ward_id', {
    preHandler: [
      fastify.guard.role('admin','doctor','nurse'),
      // fastify.guard.scope('order.create','admit.read')
    ],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    const datas:object = req.body;
    const id:any = req.params.waed_id;

    try {
      const data:any = await importService.update(db,datas,id);
      return reply.status(StatusCodes.OK).send({ ok: true ,data});
    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })  

  //Delete
  fastify.delete('/:ward_id', {
    preHandler: [
      fastify.guard.role('admin','doctor','nurse'),
      // fastify.guard.scope('order.create','admit.read')
    ],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    const id:any = req.params.waed_id;

    try {
      const data:any = await importService.delete(db,id);
      return reply.status(StatusCodes.OK).send({ ok: true ,data});
    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  }) 


  done();

} 
