import { FastifyInstance, FastifyReply, FastifyRequest } from "fastify";
import { getReasonPhrase, StatusCodes } from 'http-status-codes';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  // verify jwt token
  fastify.addHook("onRequest", (request) => request.jwtVerify());

  fastify.register(require('./activity'), { prefix: '/lookup/activity' });
  fastify.register(require('./bed'), { prefix: '/lookup/bed' });
  fastify.register(require('./user'), { prefix: '/lookup/user' });
  fastify.register(require('./evaluate'), { prefix: '/lookup/evaluate' });
  fastify.register(require('./problem_list'), { prefix: '/lookup/problem-list' });
  fastify.register(require('./standing_order'), { prefix: '/lookup/standing-order' });
  fastify.register(require('./standing_progress_note'), { prefix: '/lookup/standing-progress-note' });

  done();

} 
